# Maintainer: Mark Wagie <mark at manjaro dot org>
# Contributor: Mario Oenning <mo-son at mailbox dot org>

pkgname=ryzen_smu-dkms
pkgver=0.1.5
pkgrel=5
pkgdesc="A Linux kernel driver that exposes access to the SMU (System Management Unit) for certain AMD Ryzen Processors."
arch=('x86_64')
url="https://gitlab.com/leogx9r/ryzen_smu"
license=('GPL-2.0-or-later')
depends=('dkms')
source=("https://gitlab.com/leogx9r/ryzen_smu/-/archive/v$pkgver/ryzen_smu-v$pkgver.tar.gz"
        'https://gitlab.com/leogx9r/ryzen_smu/-/merge_requests/12.patch'
        'hawkpoint.patch'
        'dkms.conf'
        'ryzen_smu.conf')
sha256sums=('25c7e6885b14fcad27430aa3fa273009c889794158d94f1b3040ad432b94d037'
            '5dc79846385ff0fe8a67cc4145d272dfc82fd311d1a5eec7f591204cb97b2a24'
            'e73a86b9b9ed59d58da86489c4348a1651e8091b24c19c25db513e3373627afb'
            '0ced27963dac8e270221574d0c2b686bbbb012e3af8741188800f9cb6b7d8e63'
            'a48f5c846ca5fd8f185f1317c8001c97f59ac432392d53b581802c7761b9360f')

prepare() {
  cd "ryzen_smu-v$pkgver"
  # Add Rembrand and Phoenix support 
  patch -p1 -i ../12.patch
  patch -p1 -i ../hawkpoint.patch
}

build() {
  cd "ryzen_smu-v$pkgver"
  make -C userspace
}

package() {
  cd "ryzen_smu-v$pkgver"
  install -d "$pkgdir/usr/src/ryzen_smu-$pkgver"
  install -Dm644 {Makefile,dkms.conf,drv.c,smu.c,smu.h} -t "$pkgdir/usr/src/ryzen_smu-$pkgver/"
  install -Dm644 "$srcdir/dkms.conf" -t "$pkgdir/usr/src/ryzen_smu-$pkgver/"

  sed -e "s/@_PKGBASE@/ryzen_smu/" \
    -e "s/@PKGVER@/$pkgver/" \
    -i "$pkgdir"/usr/src/ryzen_smu-$pkgver/dkms.conf

  install -Dm644 "$srcdir/ryzen_smu.conf" -t "$pkgdir/usr/lib/depmod.d/"
  install -Dm700 userspace/monitor_cpu -t "$pkgdir/usr/bin/"
}
